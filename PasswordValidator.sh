#!/bin/bash
#=====================================================================================
# This is a bash password validation script.
# The script should receive one input argument only.
# The password must:
# 1. Be at least 10 charcters long.
# 2. Contain only alphanumeric characters.
# 3. Contain at least 1 lowercase Charcter.
# 4. Contain at least 1 lowercase Charcter.
# 5. Contain at least 1 numeric Charcter.
#
# Detailed output Error messages are printed in RED color to the user in case 
# any of the above is violated. Success output message is printed in GREEN if 
# all validation pass successfully.
#=====================================================================================
# Setting to expand aliases (enabling aliases defined in the script)
shopt -s expand_aliases

alias echo='echo -e'

# Number of arguments provided to the script.
numOfArgs=$#

# Capturing the argument provided -  the password.
inputPassword=$1

# Variables for needed colors
RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[0;32m'

#=================================================================
# This function will print in RED color how the script should be 
# executed, along with an example.
#=================================================================
function printUsage_andExit1 {
	echo 
	echo "${RED}Usage:"
	echo "		$0 <\"Password\">"
	echo "Example:"
	echo "		$0 \"MyPa55w0rd\""
	echo
	echo "Please try again..."
	echo "Exiting..."	
	echo
	exit 1
}

#=================================================================
# Function that validates the the script was executed with exactly 1 argument.
# If not argument provided, or more then 1 argument provided, 
# an appropriate error message will be printed in RED.
#=================================================================
function check_input {

	if [ $numOfArgs -eq 0 -o $numOfArgs -gt 1 ] 
	then
		echo "${RED}ERROR: Either No Arguments provided, or more than 1 argument provided..."
		printUsage_andExit1
	fi
}

#=================================================================
# Function that validates the length of the password,
# which should be at least 10 characters.
# If less than 10 characters, appropriate error message is printed in RED.
#=================================================================
function validate_length {

	inputLength=${#inputPassword}

	if [ "${inputLength}" -lt "10" ]
	then
		echo
		echo "${RED}ERROR: Password length is:${inputLength}"
		echo "ERROR: Password should be at least 10 characters..."
		printUsage_andExit1
	fi
}

#=================================================================
# Function that validates that the password contains only alphanumeric chars.
# Otherwise an appropriate error message is printed in RED.
#=================================================================
function validate_alphanumeric {

	if [[ ! $inputPassword =~ ^[a-zA-Z0-9]+$ ]]
	then
		echo "${RED}ERROR: Password includes non Alphanumeric..."
		printUsage_andExit1
	fi
}

#=================================================================
# Function that validates that the password contains at least 1 lowercase char.
# Otherwise an appropriate error message is printed in RED.
#=================================================================
function validate_lowercase {

	if [[ ! $inputPassword =~ [a-z] ]]
	then
		echo "${RED}ERROR: Password should contain at least 1 LOWERCASE letter..."
		printUsage_andExit1
	fi
}

#=================================================================
# Function that validates that the password contains at least 1 uppercase char.
# Otherwise an appropriate error message is printed in RED.
#=================================================================
function validate_uppercase {

	if [[ ! $inputPassword =~ [A-Z] ]]
	then
		echo "${RED}ERROR: Password should contain at least 1 UPPERCASE letter..."
		printUsage_andExit1
	fi
}

#=================================================================
# Function that validates that the password contains at least 1 numeric char.
# Otherwise an appropriate error message is printed in RED.
#=================================================================
function validate_numeric {

	if [[ ! $inputPassword =~ [0-9] ]]
	then
		echo "${RED}ERROR: Password should contain at least 1 NUMERIC character..."
		printUsage_andExit1
	fi
}

#=================================================================
# Function that checks the input password composition, by calling 
# respective functions, each responsible for its own validation.
# Checks if password, is alphanumeric, contains lowercase, uppercase and numeric.
#=================================================================
function validate_composition {
	validate_alphanumeric	
	validate_lowercase 
	validate_uppercase 
	validate_numeric 

}

#=================================================================
# Function that validates the input password length and composition, by calling 
# respective functions, each responsible for its own validation.
# Checks password Length (!<10) and composition (is alphanumeric, contains lowercase, 
# uppercase and numeric).
#=================================================================
function validate_password {
	validate_length
	validate_composition
}

#=================================================================
############################# MAIN ###############################
#=================================================================
# 1. Check that the script executed properly, by checking the input provided to it.
# 2. Valiadte the password. 
#=================================================================
check_input
validate_password

echo "${GREEN}Valid Password - Thank you!!"
echo
exit 0


